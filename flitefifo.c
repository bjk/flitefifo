/*
    Copyright (C) 2009-2018
    Ben Kibbey <bjk@luxsci.net>

    This file is part of flitefifo.

    flitefifo is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    flitefifo is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with flitefifo.  If not, see <http://www.gnu.org/licenses/>.
*/
#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <err.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <regex.h>
#include <pwd.h>
#include <fcntl.h>
#include <limits.h>
#include <signal.h>
#include <ctype.h>
#include <syslog.h>
#include <pthread.h>
#include <stdint.h>

#ifdef WITH_FLITE
#include <flite/flite.h>
extern cst_voice *register_cmu_us_kal ();
#endif

#ifdef WITH_ESPEAK
#include <espeak/speak_lib.h>
#endif

#ifdef WITH_PA
#include <pulse/pulseaudio.h>
#include <pulse/simple.h>
#endif

/* Maximum number of sub-expressions per expression in the rcfile. */
#define MAX_SUBEXP	9

/* A linked list of regular expressions which are read from the rcfile. */
struct exp_s
{
  regex_t re;
  char *subexp;
  struct exp_s *next;
};

/* A linked list of FIFO's to watch. */
struct fifo_s
{
  char *name;
  int fd;
  char **lines;
  unsigned nlines;
  char *prefix;
  char *suffix;
  struct exp_s *exp;
  struct fifo_s *next;
};

static struct fifo_s *fifos;
static int quit, reloadsig;
static pthread_mutex_t mutex;
static char *rcfile;
static int background;
#ifdef WITH_PA
static pa_simple *pa_handle;
static int with_pulse;
#endif
#ifdef WITH_FLITE
cst_voice *voice;
#endif
static int with_flite;
static int cmdline;

static void freeFifoList (struct fifo_s *list);
static int createFifos (struct fifo_s *list);
static struct fifo_s *parse_rcfile ();

static void
usage (const char *pn)
{
  fprintf (stderr,
#ifdef WITH_PA
#ifdef WITH_FLITE
	   "Usage: %s [-hv] [-n] [-P] [-F] [-r <role>] [-f <rcfile>]\n"
#else
	   "Usage: %s [-h] [-n] [-P] [-r <role>] [-f <rcfile>]\n"
#endif
#else
#ifdef WITH_FLITE
	   "Usage: %s [-h] [-n] [-F] [-f <rcfile>]\n"
#else
	   "Usage: %s [-h] [-n] [-f <rcfile>]\n"
#endif
#endif
	   "   -n  dont fork into background\n"
	   "   -f  alternate rcfile (~/.flitefiforc)\n"
#ifdef WITH_PA
	   "   -P  output to pulseaudio\n"
           "   -r  pulseaudio media role\n"
#endif
#ifdef WITH_FLITE
	   "   -F  use flite as the speech engine\n"
#endif
	   "   -v  version info\n"
           "   -h  this help text\n", pn);
}

static void
logit (const char *fmt, ...)
{
  va_list ap;
  char *buf;

  va_start (ap, fmt);
  vasprintf (&buf, fmt, ap);
  va_end (ap);

  if (!background || cmdline)
    fprintf(stderr, "flitefifo: %s\n", buf);
  else
    syslog (LOG_INFO, "%s", buf);

  free (buf);
}

#if defined(WITH_PA) && defined(WITH_ESPEAK)
static int
synth_cb (short *wav, int n_samples, espeak_EVENT * ev)
{
  static unsigned char *data;
  static unsigned data_len;
  int pa_error;
  size_t len;

  if (!with_pulse)
    return 0;

  switch (ev->type)
    {
    case espeakEVENT_LIST_TERMINATED:
      if (!data)
	break;

      pa_simple_write (pa_handle, data, data_len, &pa_error);
      pa_simple_drain (pa_handle, &pa_error);
      free (data);
      data = NULL;
      data_len = 0;
      break;
    case espeakEVENT_END:
    case espeakEVENT_SENTENCE:
    case espeakEVENT_WORD:
      len = n_samples * sizeof (short);
      data = realloc (data, data_len + len);
      memcpy (&data[data_len], wav, len);
      data_len += len;
      break;
    default:
      free (data);
      data = NULL;
      data_len = 0;
      break;
    }

  return 0;
}
#elif defined (WITH_ESPEAK)
static int
synth_cb (short *wav, int n_samples, espeak_EVENT * ev)
{
  return 0;
}
#endif

#ifdef WITH_FLITE
static int
play_flite (const char *str)
{
#ifdef WITH_PA
  if (with_pulse)
    {
      int pa_error;
      cst_wave *w = flite_text_to_wave (str, voice);
      cst_wave_resample (w, 22000);
      size_t len = w->num_samples * sizeof (uint16_t);
      unsigned char *buf;

      buf = malloc (len);
      memcpy (buf, w->samples, len);
      delete_wave (w);
      pa_simple_write (pa_handle, buf, len, &pa_error);
      pa_simple_drain (pa_handle, &pa_error);
      free (buf);
      return 0;
    }
#endif

  flite_text_to_speech (str, voice, "play");
  return 0;
}
#endif

#ifdef WITH_ESPEAK
static int
play_espeak (const char *str)
{
  espeak_Synth (str, strlen (str), 0, POS_CHARACTER, 0,
		espeakCHARS_AUTO | espeakSSML, NULL, NULL);
  return 0;
}
#endif

static int
reload ()
{
  freeFifoList (fifos);
  fifos = parse_rcfile ();

  if (!fifos)
    return 1;

  return createFifos (fifos);
}

static void
freeLines (struct fifo_s *f)
{
  char **p;

  if (!f->lines)
    return;

  for (p = f->lines; *p; p++)
    free (*p);

  free (f->lines);
  f->lines = NULL;
  f->nlines = 0;
}

static void *
parse_subexp (void *arg)
{
  struct fifo_s *fifo = arg;
  struct exp_s *e;
  char buf[LINE_MAX] = { 0 }, *b = buf;
  regmatch_t pmatch[MAX_SUBEXP];
  int match = 0;

  if (fifo->prefix)
    snprintf (buf, sizeof (buf), "%s", fifo->prefix);

  b = buf + strlen (buf);
  pthread_mutex_lock (&mutex);

  for (e = fifo->exp; e; e = e->next)
    {
      char *p;

      if (regexec (&e->re, fifo->lines[0], MAX_SUBEXP, pmatch, 0) ==
	  REG_NOMATCH)
	continue;

      match = 1;
      if (e->subexp)
	{
	  for (p = e->subexp; *p; p++)
	    {
	      if (*p == '\\')
		{
		  if (isdigit (*(p + 1)))
		    {
		      int i;
		      int n = atoi (++p);

		      for (i = pmatch[n].rm_so; i < pmatch[n].rm_eo; i++)
			*b++ = fifo->lines[0][i];

		      continue;
		    }
		}

	      *b++ = *p;
	    }

	  *b = 0;

	  if (buf[0])
	    break;
	}
      else
	{
	  strncat (buf, fifo->lines[0], sizeof (buf) - 1);
	  break;
	}
    }

  freeLines (fifo);

  if (fifo->suffix)
    strncat (buf, fifo->suffix, sizeof (buf) - 1);

  if (buf[0] && match)
    {
#ifdef WITH_FLITE
      if (with_flite)
	play_flite (buf);
#endif
#ifdef WITH_ESPEAK
      if (!with_flite)
	play_espeak (buf);
#endif
    }

  pthread_mutex_unlock (&mutex);
  return NULL;
}

static void
loop ()
{
  pthread_t parser_tid;

  while (!quit)
    {
      struct fifo_s *f;
      fd_set rfds;
      int n = 0;
      struct timeval tv = { 0, 100000 };

      if (reloadsig)
	{
	  reloadsig = 0;

	  if (reload ())
	    break;
	}

      FD_ZERO (&rfds);

      for (f = fifos; f; f = f->next)
	{
	  FD_SET (f->fd, &rfds);
	  n = f->fd > n ? f->fd : n;
	}

      n = select (n + 1, &rfds, NULL, NULL, &tv);

      if (n == -1 && errno != EINTR)
	{
          logit ("%s", strerror (errno));
	  break;
	}

      if (n <= 0)
	continue;

      for (f = fifos; f; f = f->next)
	{
	  if (FD_ISSET (f->fd, &rfds))
	    {
	      char buf[LINE_MAX], *line;
	      void *value;
	      FILE *fp = fdopen (f->fd, "r");

	      while ((line = fgets (buf, sizeof (buf), fp)) != NULL)
		{
		  if (line[strlen (line) - 1] == '\n')
		    line[strlen (line) - 1] = 0;

		  f->lines =
		    realloc (f->lines, f->nlines + 2 * sizeof (char *));
		  f->lines[f->nlines++] = strdup (line);
		  f->lines[f->nlines] = NULL;
		  pthread_create (&parser_tid, NULL, parse_subexp, f);
		  pthread_join (parser_tid, &value);

		  // FIXME prioritize based on order in the rcfile. the
		  // priority is a preference of what to play when there are
		  // more than some specified amount of buffered lines. and
		  // determines what gets flushed.
		  while ((fgets (buf, sizeof (buf), fp)));
		  break;
		}

	      if (feof (fp))
		{
		  close (f->fd);
		  f->fd = open (f->name, O_RDONLY | O_NONBLOCK);
		}
	    }
	}
    }
}

static void
freeExpList (struct exp_s *list)
{
  struct exp_s *e;

  for (e = list; e;)
    {
      struct exp_s *tmp;

      regfree (&e->re);

      if (e->subexp)
	free (e->subexp);

      tmp = e->next;
      free (e);
      e = tmp;
    }
}

static char *
expand_homedir (const char *str)
{
  const char *p = str;

  if (*p++ == '~')
    {
      struct passwd *pw = getpwuid (getuid ());
      char buf[2048];

      snprintf (buf, sizeof (buf), "%s%s", pw->pw_dir, p);
      return strdup (buf);
    }

  return strdup (str);
}

static struct fifo_s *
parse_rcfile ()
{
  char buf[LINE_MAX], *l;
  FILE *fp = fopen (rcfile, "r");
  int line = 1, n_exp = 0;
  struct fifo_s *newlist = NULL, *f = newlist;
  int first_file = 1;

  if (!fp)
    {
      if (background)
	syslog (LOG_INFO, "%s", rcfile);
      else
	warn ("%s", rcfile);

      return NULL;
    }

  while ((l = fgets (buf, sizeof (buf), fp)))
    {
      if (*l == '#')
	{
	  line++;
	  continue;
	}

      if (l[strlen (l) - 1] == '\n')
	l[strlen (l) - 1] = 0;

      if (*l == '$')
	{
	  if (strchr (++l, '='))	// configuration option
	    {
	      if (!strncmp (l, "prefix=", 7))
		{
		  f->prefix = strdup (l + 7);
		}
	      else if (!strncmp (l, "suffix=", 7))
		{
		  f->suffix = strdup (l + 7);
		}
	      else
		warnx ("%s[%i]: unknown config option", rcfile, line);

	      line++;
	      continue;
	    }

	  if (!newlist)
	    {
	      newlist = calloc (1, sizeof (struct fifo_s));
	      f = newlist;
	    }
	  else
	    {
              logit ("%s: parsed %i expression(s)", f->name, n_exp);
	      first_file = n_exp = 0;
	      f->next = calloc (1, sizeof (struct fifo_s));
	      f = f->next;
	    }

	  f->name = expand_homedir (l);
	  line++;
	  continue;
	}

      if (!f)
	{
	  warnx ("%s[%i]: Missing initial filename section", rcfile, line);
	  goto fail;
	}

      char *p = strchr (l, '\t');
      regex_t re;
      int n;
      char errbuf[255];
      struct exp_s *new, *e;
      int split = 0;

      if (!p)
	p = l;
      else
	{
	  p = strsep (&l, "\t");
	  split = 1;
	}

      if (!*p)
	continue;

      n = regcomp (&re, p, REG_EXTENDED);

      if (n)
	{
	  regerror (n, &re, errbuf, sizeof (errbuf));

	  if (background)
	    syslog (LOG_INFO, "%s[%i]: %s", rcfile, line, errbuf);
	  else
	    warnx ("%s[%i]: %s", rcfile, line, errbuf);

	  goto fail;
	}

      new = calloc (1, sizeof (struct exp_s));
      new->re = re;

      if (split && l)
	new->subexp = strdup (l);

      if (!f->exp)
	f->exp = new;
      else
	{
	  for (e = f->exp; e; e = e->next)
	    {
	      if (!e->next)
		{
		  e->next = new;
		  e = e->next;
		}
	    }
	}

      n_exp++;
      line++;
    }

  if (line == 1)
    {
      if (background)
	syslog (LOG_INFO, "parsed %i expression(s)", line);
      else
	warnx ("%s: no expressions to parse, exiting", rcfile);

      goto fail;
    }

  if (!first_file)
    logit ("%s: parsed %i expression(s)", f->name, n_exp);

  fclose (fp);
  return newlist;

fail:
  freeFifoList (newlist);
  fclose (fp);
  return NULL;
}

static void
closeFifos (struct fifo_s *list)
{
  struct fifo_s *f;

  for (f = list; f; f = f->next)
    {
      if (f->fd >= 0)
	close (f->fd);

      f->fd = -1;
    }
}

static void
freeFifoList (struct fifo_s *list)
{
  struct fifo_s *f;

  closeFifos (list);

  for (f = list; f;)
    {
      struct fifo_s *tmp;

      if (f->name)
	{
	  unlink (f->name);
	  free (f->name);
	}

      freeLines (f);
      freeExpList (f->exp);
      tmp = f->next;
      free (f->prefix);
      free (f->suffix);
      free (f);
      f = tmp;
    }
}

static void
catchsig (int sig)
{
  switch (sig)
    {
    case SIGTERM:
    case SIGINT:
      quit = 1;
      break;
    case SIGHUP:
      reloadsig = 1;
      break;
    default:
      break;
    }
}

static int
createFifos (struct fifo_s *list)
{
  struct fifo_s *f;

  for (f = list; f; f = f->next)
    {
      if (mkfifo (f->name, 0600) == -1)
	{
	  if (errno == EEXIST)
	    {
	      struct stat st;

	      stat (f->name, &st);

	      if (!S_ISFIFO (st.st_mode))
		{
		  warn ("%s", f->name);
		  goto fail;
		}
	    }
	  else
	    {
	      warn ("%s", f->name);
	      goto fail;
	    }
	}

      f->fd = open (f->name, O_RDONLY | O_NONBLOCK);

      if (f->fd == -1)
	{
	  warn ("%s", f->name);
	  goto fail;
	}
    }

  return 0;

fail:
  return 1;
}

int
main (int argc, char **argv)
{
  int opt;
  struct passwd *pw = getpwuid (getuid ());
  char buf[PATH_MAX];
#ifdef WITH_PA
  const char *role = "";
#endif

  cmdline = background = 1;
  snprintf (buf, sizeof (buf), "%s/.flitefiforc", pw->pw_dir);
  rcfile = strdup (buf);
  signal (SIGTERM, catchsig);
  signal (SIGINT, catchsig);
  signal (SIGHUP, catchsig);
  openlog ("flitefifo", LOG_PID, LOG_DAEMON);
  pthread_mutex_init (&mutex, NULL);

#ifdef WITH_PA
#ifdef WITH_FLITE
  while ((opt = getopt (argc, argv, "nhvf:PFr:")) != -1)
    {
#else
  while ((opt = getopt (argc, argv, "nhvf:Pr:")) != -1)
    {
#endif
#else
#ifdef WITH_FLITE
  while ((opt = getopt (argc, argv, "nhvf:F")) != -1)
    {
#else
  while ((opt = getopt (argc, argv, "nhvf:")) != -1)
    {
#endif
#endif
      switch (opt)
	{
#ifdef WITH_PA
	case 'P':
	  with_pulse = 1;
	  break;
	case 'r':
	  role = optarg;
	  break;
#endif
	case 'F':
	  with_flite = 1;
	  break;
	case 'n':
	  background = 0;
	  break;
	case 'f':
	  free (rcfile);
	  rcfile = strdup (optarg);
	  break;
	case 'v':
	  printf ("flitefifo %s\nCopyright 2009-2014 %s\n",
		  PACKAGE_VERSION, PACKAGE_BUGREPORT);
	  exit (EXIT_SUCCESS);
	  break;
	case 'h':
	  usage (argv[0]);
	  exit (EXIT_SUCCESS);
	}
    }

  fifos = parse_rcfile ();

  if (!fifos)
    {
      free (rcfile);
      exit (EXIT_FAILURE);
    }

#ifdef WITH_FLITE
  if (with_flite)
    {
      flite_init ();
      voice = register_cmu_us_kal ();
    }
#endif

  if (background)
    daemon (0, 0);

  cmdline = 0;
#ifdef WITH_ESPEAK
  if (!with_flite)
    {
#ifdef WITH_PA
      if (with_pulse)
	espeak_Initialize (AUDIO_OUTPUT_SYNCHRONOUS, 8196, NULL, 0);
      else
	espeak_Initialize (AUDIO_OUTPUT_SYNCH_PLAYBACK, 8196, NULL, 0);
#else
      espeak_Initialize (AUDIO_OUTPUT_SYNCH_PLAYBACK, 8196, NULL, 0);
#endif
      espeak_SetSynthCallback (synth_cb);
    }
#endif

  if (createFifos (fifos))
    goto fail;

#ifdef WITH_PA
  if (with_pulse)
    {
      pa_sample_spec ss;

      memset (&ss, 0, sizeof (ss));
      setenv ("PULSE_PROP_application.name", "Flite FIFO", 1);
      //setenv("PULSE_PROP_application.icon_name", "flitefifo", 1);
      setenv ("PULSE_PROP_media.role", role, 1);
      ss.format = PA_SAMPLE_S16NE;
      ss.channels = 1;
      ss.rate = 22050;
      pa_handle = pa_simple_new (NULL, "flitefifo", PA_STREAM_PLAYBACK, NULL,
				 "TextToSpeech", &ss, NULL, NULL, NULL);

      if (!pa_handle)
	{
	  warnx ("pa_simple_new() failed");
	  goto fail;
	}
    }
#endif

  logit ("%s", "started");
  loop ();
  freeFifoList (fifos);
  pthread_mutex_destroy (&mutex);

  logit ("%s", "exiting");

#if WITH_PA
  if (with_pulse)
    pa_simple_free (pa_handle);
#endif

  free (rcfile);
  exit (EXIT_SUCCESS);

fail:
  free (rcfile);
  exit (EXIT_FAILURE);
}
